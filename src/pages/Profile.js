import React from 'react'
import { useLocation } from 'react-router'
import andLogo from '../assets/and-digital.png'
import './Profile.css'
import profileImg from '../assets/profile.jpeg'

function Profile() {
    const location = useLocation()
    const { profile } = location.state
    const { experience, keyClients, industryExpertise, tools, name, jobTitle, andTitle, overview, coreFocus, keySkills } = profile

    return (
        <div className='profile'>
            <div className="details">
                <h2>Experience Highlights</h2>
                <section className="experience">
                    {experience.map((exp, i) => (
                        <div key={i}>
                            <h3>{exp.role}</h3>
                            <h4>{exp.company}</h4>
                            <p>{exp.summary}</p>
                        </div>
                    ))}
                </section>
                <section className="lists">
                    <div>
                        <h2>KEY CLIENTS</h2>
                        <ul>{keyClients.map(client => <li key={client}>{client}</li>)}</ul>
                    </div>
                    <div>
                        <h2>INDUSTRY EXPERTISE</h2>
                        <ul>{industryExpertise.map(industry => <li key={industry}>{industry}</li>)}</ul>
                    </div>
                    <div>
                        <h2>TOOLS</h2>
                        <ul>{tools.map(tool => <li key={tool}>{tool}</li>)}</ul>
                    </div>
                </section>
            </div>
            <div className='overview'>
                <img src={profileImg} alt={name} className="profile-img" />
                <div>
                    <h1>{name}</h1>
                    <p>{jobTitle}</p>
                    <p>AND {andTitle}</p>
                    <p>-</p>
                    <h2>OVERVIEW</h2>
                    <p>{overview}</p>
                    <p>-</p>
                    <h2>CORE FOCUS</h2>
                    {coreFocus.map(focus => <p key={focus}>{focus}</p>)}
                    <p>-</p>
                    <h2>KEY SKILLS</h2>
                    {keySkills.map(skill => <span key={skill}>{skill}</span>)}
                </div>
                <img src={andLogo} alt="and digital logo" className="and-logo" />
            </div>
        </div>
    )
}

export default Profile