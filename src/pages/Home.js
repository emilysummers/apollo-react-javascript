import React, { useState, useEffect } from 'react'
import './Home.css'
import Search from '../components/Search'
import Filter from '../components/Filter'
import Bio from '../components/Bio'
import data from '../lib/seed.json'
import { Button, Container, PageHeader, Box, Flex } from 'and-design-components-library'

function Home() {
    const [allBios, setAllBios] = useState([])

    useEffect(() => {
        getAllBios()
        getAllClubs()
        getAllRoles()
        getAllSquads()
        getAllTools()
    }, [])

    const getAllBios = () => {
        // fetch('http://localhost:8080/bios')
        //     .then(response => response.json())
        //     .then(data => {
        //         setAllBios(data)
        //         const clubs = data.map(bio => bio.club)
        //         setAllClubs([... new Set(clubs)])
        //     })
        setAllBios(data)
    }

    // gets all ANDi clubs and stores in state
    const [allClubs, setAllClubs] = useState([])

    const getAllClubs = () => {
        const clubs = data.map(bio => bio.club)
        setAllClubs([...new Set(clubs)])
    }

    // gets all ANDi roles and stores in state
    const [allRoles, setAllRoles] = useState([])
    const getAllRoles = () => {
        const roles = data.map(bio => bio.jobTitle)
        setAllRoles([...new Set(roles)])
    }

    // gets all ANDi squads and stores in state
    const [allSquads, setAllSquads] = useState([])

    const getAllSquads = () => {
        const squads = data.map(bio => bio.squad)
        setAllSquads([...new Set(squads)])
    }

    // gets all ANDi tools and stores in state
    const [allTools, setAllTools] = useState([])

    const getAllTools = () => {
        const toolsLists = data.map(bio => bio.tools)
        let tools = []
        toolsLists.forEach(toolsList => tools = [...tools, ...toolsList])
        setAllTools([...new Set(tools)])
    }

    const searchBios = async (searchText) => {
        searchText ?
            await fetch(`http://localhost:8080/bios?filter=${searchText}`)
                .then(response => response.json())
                .then(data => {
                    data.length > 0 ?
                        setAllBios(data)
                        :
                        setAllBios(null)
                })
            :
            getAllBios()
    }

    // stores selected dropdown data in state
    const [filterData, setFilterData] = useState({ club: '', jobTitle: '', squad: '', tool: '' })

    const handleChange = ({ target: { name, value } }) => {
        setFilterData({ ...filterData, [name]: value })
    }

    // filters through all bios based on filter data and stores in state a list of bios that match any of the fields
    const filterBios = (e) => {
        e.preventDefault()
        const filteredBios =
            data
                .filter(bio => filterData.club ? bio.club === filterData.club : bio.club)
                .filter(bio => filterData.jobTitle ? bio.jobTitle === filterData.jobTitle : bio.jobTitle)
                .filter(bio => filterData.squad ? bio.squad === filterData.squad : bio.squad)
                .filter(bio => filterData.tool ? bio.tools.includes(filterData.tool) : bio.tools)
        setAllBios(filteredBios)
    }

    return (
        <Container width={1400}>
            <Box my={20}>
                <PageHeader
                    title='ANDi Directory'
                    subtitle='Search and filter ANDi bios'
                    titleColor='red'
                    subtitleColor='greyMid'
                />
            </Box>
            <Search allBios={allBios} searchBios={searchBios} />
            <Filter
                filterBios={filterBios}
                filterData={filterData}
                handleChange={handleChange}
                clubs={allClubs}
                roles={allRoles}
                squads={allSquads}
                tools={allTools}
            />
            <Flex justifyContent='center'>
                <Box width={1400}>
                    {allBios ?
                        allBios.map(profile => <Bio profile={profile} key={profile.urn} />)
                        :
                        <h5>No bios match your search.</h5>}
                </Box>
            </Flex>
            <Flex justifyContent='center' mt={20} mb={60}>
                <Button>Load more</Button>
            </Flex>
        </Container>
    )
}

export default Home