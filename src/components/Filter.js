import React from 'react'
import './Filter.css'
import { Button, Dropdown, Flex } from 'and-design-components-library'

function Filter({ filterBios, filterData, handleChange, clubs, roles, squads, tools }) {
  return (
    <Flex alignItems='center'>
      <form onSubmit={(e) => filterBios(e)}>
        <Flex width={1100} mb={40} mt={10} alignItems='center' justifyContent='space-between'>
          <Dropdown
            width={227}
            onChange={handleChange}
            name="club"
            defaultValue={filterData.club}
          >
            <option disabled value={filterData.club}>Select club</option>
            {clubs.map(club => <option key={club} value={club}>{club}</option>)}
          </Dropdown>
          <Dropdown
            width={227}
            onChange={handleChange}
            name="jobTitle"
            defaultValue={filterData.jobTitle}
          >
            <option disabled value={filterData.jobTitle}>Select role</option>
            {roles.map(role => <option key={role} value={role}>{role}</option>)}
          </Dropdown>
          <Dropdown
            width={227}
            onChange={handleChange}
            name="squad"
            defaultValue={filterData.squad}
          >
            <option disabled value={filterData.squad}>Select squad</option>
            {squads.map(squad => <option key={squad} value={squad}>{squad}</option>)}
          </Dropdown>
          <Dropdown
            width={227}
            onChange={handleChange}
            name="tool"
            defaultValue={filterData.tool}
          >
            <option disabled value={filterData.tool}>Select tool</option>
            {tools.map(tool => <option key={tool} value={tool}>{tool}</option>)}
          </Dropdown>
          <Button type="submit">Apply filters</Button>
        </Flex>
      </form>
      <Flex mb={40} mt={10} alignItems='center'>
        <form>
          <Button type="submit">Clear filters</Button>
        </form>
      </Flex>
    </Flex>
  )
}

export default Filter