import React from 'react'
import './Bio.css'
import { Link } from 'react-router-dom'
import { BioCardRow } from 'and-design-components-library'

function Bio({ profile }) {
    const { slug, name, jobTitle, level, club, squad, tools, imgUrl } = profile

    return (
        <Link to={`/bios/${slug}`} state={{ profile }}>
            <BioCardRow
                name={name}
                jobTitle={jobTitle}
                level={level}
                club={club}
                squad={squad}
                tools={tools}
                avatarUrl={imgUrl}
            />
        </Link>
    )
}

export default Bio