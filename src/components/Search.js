import React from 'react'
import './Search.css'
import { Box, SearchBarWithIcon } from 'and-design-components-library'

function Search({ searchBios }) {
    return (
        <Box width={600}>
            <SearchBarWithIcon
                placeholder="Search by first and/or last name"
                onChange={(event) => searchBios(event.target.value)}
            />
        </Box>
    )
}

export default Search