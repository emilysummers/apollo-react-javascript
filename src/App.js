import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Home from './pages/Home'
import Profile from './pages/Profile'

function App() {
  return (
      <BrowserRouter>
        <Routes>
          <Route exact path="/bios" element={<Home />} />
          <Route path='/bios/:slug' element={<Profile />} />
        </Routes>
      </BrowserRouter>
  )
}

export default App
